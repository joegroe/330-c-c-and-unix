# Poor Mans Photoshop!

This project was completed through the entirety of CIS 330 at the Unveristy of Oregon.

This class taught proper C/C++ programming while working in a strictly Unix based system.

### Setting Up
To run this program, download the repository and use the makefile provided.

Inside the repository, there are sample inputs in pnm format.

Both input and output files must be in .pnm format.
```bash
$ make
$ ./proj3G input.file output.file
```

Below is the sample input and output that the program produces.
![input](https://bitbucket.org/joegroe/330-c-c-and-unix/raw/327fcde10a0ef935e024324a43972036b083e327/img/input.png)

![output](https://bitbucket.org/joegroe/330-c-c-and-unix/raw/327fcde10a0ef935e024324a43972036b083e327/img/output.png)


